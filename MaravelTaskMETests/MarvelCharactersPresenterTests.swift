//
//  MarvelCharactersPresenterTests.swift
//  MaravelTaskMETests
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import XCTest
import Quick
import Nimble
import SwiftyJSON

@testable import MaravelTaskME

class MarvelCharactersPresenterTests: QuickSpec {
    
    class Fake_CharactersPresenter: CharactersPresenter {
        
        var isGetCharactersCalled = false
        var fakeResult: GetCharactersResponse?
        var error: NSError?
        
        override func getAllCharacters(offset: Int, onSuccess: @escaping (GetCharactersResponse) -> Void, onFailure: @escaping () -> Void) {
            
            if error == nil {
                isGetCharactersCalled = true
                onSuccess(fakeResult!)
            }else {
                isGetCharactersCalled = false
                onFailure()
            }
            
        }
    }
    
    override func spec() {
        
        var ctrl: MarvelCharactersViewController!
        
        describe("MarvelCharactersViewControllerSpec") {
            beforeEach {
                ctrl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MarvelCharactersViewController") as! MarvelCharactersViewController
                
                _ = ctrl.view
            }
            
            context("when view is loaded") {
                it("should have 0 character loaded") {
                    expect(ctrl.tableView.numberOfRows(inSection: 0)).to(equal(0))
                    expect(ctrl.charactersArray.count).to(equal(0))
                }
            }
        }
        
        describe("getAllCharacters") {
            context("Characters are fetched successfully") {
                it("sets the Characters as the data source") {
                    
                    let fakePresenter = Fake_CharactersPresenter()
                    ctrl.presenter = fakePresenter
                    
                    let testBundle = Bundle(for: type(of: self))
                    let path = testBundle.path(forResource: "characters", ofType: "json")
                    let dataJsonFile = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
                    
                    var response: JSON?
                    do {
                        try response = JSON(data: dataJsonFile!)
                    }
                    catch {
                        print("Something went wrong!")
                    }
                    
                    print(response!)
                    if let data = response!["data"].dictionary {
                        
                        fakePresenter.fakeResult = GetCharactersResponse(parametersJson: data)
                    }
                    
                    ctrl.getCharacters()
                    
                    expect(fakePresenter.error).toEventually(beNil())
                    expect(fakePresenter.isGetCharactersCalled).to(beTrue())
                    expect(ctrl.charactersArray.count).to(beGreaterThan(4))
                    expect(ctrl.tableView.numberOfRows(inSection: 0)).to(beGreaterThan(4))
                }
            }
            
            
            context("Characters returned Error") {
                it("return the error") {
                    
                    let fakePresenter = Fake_CharactersPresenter()
                    ctrl.presenter = fakePresenter
                    
                    let testBundle = Bundle(for: type(of: self))
                    let path = testBundle.path(forResource: "characters", ofType: "json")
                    let dataJsonFile = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
                    
                    var response: JSON?
                    do {
                        try response = JSON(data: dataJsonFile!)
                    }
                    catch {
                        print("Something went wrong!")
                    }
                    
                    print(response!)
                    if let data = response!["dataa"].dictionary {
                        fakePresenter.fakeResult = GetCharactersResponse(parametersJson: data)
                    }else {
                        let error = NSError(domain: "Not Authorized", code: 404, userInfo: nil)
                        fakePresenter.error = error
                    }
                    
                    ctrl.getCharacters()
                    
                    expect(fakePresenter.error).toEventuallyNot(beNil())
                    expect(fakePresenter.isGetCharactersCalled).to(beFalse())
                }
            }
        }
    }
}

