//
//  UserDefaults.swift
//  Fashion
//
//  Created by Mahmoud Fathelbab on 10/27/17.
//  Copyright © 2017 FashionOnWheels. All rights reserved.
//

import UIKit

func saveObjectLocal(_ object: AnyObject, key: String)
{
    let defaults: UserDefaults = UserDefaults.standard
    
    defaults.set(object, forKey: key)
    defaults.synchronize()
}

func getObjectLocal(_ key: String) -> AnyObject
{
    let defaults: UserDefaults = UserDefaults.standard
    
    return defaults.object(forKey: key) as AnyObject
}


func removeObjectLocal(_ key: String)
{
    let defaults: UserDefaults = UserDefaults.standard
    defaults.removeObject(forKey: key)
}
