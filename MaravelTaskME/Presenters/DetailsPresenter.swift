//
//  DetailsPresenter.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class DetailsPresenter: NSObject {
    
    func getImageThumbnailURL(url: String, onSuccess: @escaping (String) -> Void, onFailure: @escaping () -> Void ) -> Void {
        
        let timeStamp = Date().timeIntervalSince1970
        let md5Data = MD5(string: String(timeStamp) + PRIVATEKEY + PUBLICKEY)
        
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        print("md5Hex: \(md5Hex)")
        
        ServiceManager.callAPI(url: url + "?apikey=" + PUBLICKEY + "&hash=" + md5Hex + "&ts=" + String(timeStamp), method: .get, parameters: nil , custumHeaders: nil) { (error, response) in
            
            if response != nil {
                
                let status = response!["status"].string
                let code = response!["code"].int
                
                if(status == "Ok" && code == 200) {
                    
                    if let data = response!["data"].dictionary {
                        
                        if let result = data["results"]?.array?.first?.dictionary {
                            
                            if let thumbnailDic = result["thumbnail"]?.dictionary {
                                
                                let thumbnail = thumbnailDic["path"]?.string
                                
                                if let characterImageExtension = thumbnailDic["extension"]?.string {
                                    let finalThumbnail = thumbnail! + "." + characterImageExtension
                                    
                                    onSuccess(finalThumbnail)
                                }
                            }
                        }
                    }else {
                        onFailure()
                    }
                }else {
                    onFailure()
                }
            }else {
                onFailure()
            }
        }
    }
    
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
}
