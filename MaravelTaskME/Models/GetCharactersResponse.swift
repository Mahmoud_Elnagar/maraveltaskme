//
//  GetCharactersResponse.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SwiftyJSON

struct GetCharactersResponse {
    
    var offset: Int64?
    var limit: Int64?
    var total: Int64?
    var count: Int64?
    var results: [MarvelCharacter]?
    
    init() {
        self.results = []
    }
    init(parametersJson: [String: JSON]?) {
        
        if let offset = parametersJson!["offset"]?.int64 {
            self.offset = offset
        }
        
        if let limit = parametersJson!["limit"]?.int64 {
            self.limit = limit
        }
        
        if let total = parametersJson!["total"]?.int64 {
            self.total = total
        }
        
        if let count = parametersJson!["count"]?.int64 {
            self.count = count
        }
        
        if let results = parametersJson!["results"]?.array {
            
            self.results = []
            for dic in results {
                
                let item = MarvelCharacter(parametersJson: dic.dictionary)
                self.results?.append(item)
            }
        }
    }
}
