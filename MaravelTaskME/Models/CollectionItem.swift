//
//  CollectionItem.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SwiftyJSON

class CollectionItem : NSObject {
    
    var resourceURI: String?
    var name: String?
    var imageURL: String?
    
    init(parametersJson: [String: JSON]?) {
        
        super.init()
        
        if let resourceURI = parametersJson!["resourceURI"]?.string {
            self.resourceURI = resourceURI
        }
        
        if let name = parametersJson!["name"]?.string {
            self.name = name
        }
    }
}
