//
//  Character.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SwiftyJSON

struct MarvelCharacter {
    
    var characterId: Int64?
    var characterName: String?
    var characterDescription: String?
    var modified: String?
    var thumbnail: String?
    var resourceURI: String?
    var characterImageExtension: String?
    
    var comics: MarvelCollection?
    var series: MarvelCollection?
    var stories: MarvelCollection?
    var events: MarvelCollection?
    var urls: [MarvelURL]?
    
    init() {
    }
    
    init(parametersJson: [String: JSON]?) {
        
        if let characterId = parametersJson!["id"]?.int64 {
            self.characterId = characterId
        }
        
        if let characterName = parametersJson!["name"]?.string {
            self.characterName = characterName
        }
        
        if let characterDescription = parametersJson!["description"]?.string {
            self.characterDescription = characterDescription
        }
        
        if let modified = parametersJson!["modified"]?.string {
            self.modified = modified
        }
        
        if let resourceURI = parametersJson!["resourceURI"]?.string {
            self.resourceURI = resourceURI
        }
        
        if let thumbnailDic = parametersJson!["thumbnail"]?.dictionary {
            
            if let thumbnail = thumbnailDic["path"]?.string {
                self.thumbnail = thumbnail
            }
            
            if let characterImageExtension = thumbnailDic["extension"]?.string {
                self.characterImageExtension = characterImageExtension
                self.thumbnail = self.thumbnail! + "." + characterImageExtension
            }
        }
        
        if let comics = parametersJson!["comics"]?.dictionary {
            self.comics = MarvelCollection(parametersJson: comics)
        }
        
        if let series = parametersJson!["series"]?.dictionary {
            self.series = MarvelCollection(parametersJson: series)
        }
        
        if let stories = parametersJson!["stories"]?.dictionary {
            self.stories = MarvelCollection(parametersJson: stories)
        }
        
        if let events = parametersJson!["events"]?.dictionary {
            self.events = MarvelCollection(parametersJson: events)
        }
        
        if let urls = parametersJson!["urls"]?.array {
            
            self.urls = []
            for dic in urls {
                
                let url = MarvelURL(parametersJson: dic.dictionary)
                self.urls?.append(url)
            }
        }
    }
}
