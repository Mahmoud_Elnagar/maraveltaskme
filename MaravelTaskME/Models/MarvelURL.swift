//
//  MarvelURL.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SwiftyJSON

struct MarvelURL {
    
    var type: String?
    var url: String?
    
    init(parametersJson: [String: JSON]?) {
        
        if let type = parametersJson!["type"]?.string {
            self.type = type
        }
        
        if let url = parametersJson!["url"]?.string {
            self.url = url
        }
    }
}
