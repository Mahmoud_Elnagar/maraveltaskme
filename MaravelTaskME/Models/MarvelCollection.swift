//
//  MarvelCollection.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SwiftyJSON

struct MarvelCollection {
    
    var available: Int64?
    var returned: Int64?
    var collectionURI: String?
    var items: [CollectionItem]?
    
    init(parametersJson: [String: JSON]?) {
        
        if let available = parametersJson!["available"]?.int64 {
            self.available = available
        }
        
        if let returned = parametersJson!["returned"]?.int64 {
            self.returned = returned
        }
        
        if let collectionURI = parametersJson!["collectionURI"]?.string {
            self.collectionURI = collectionURI
        }
        
        if let items = parametersJson!["items"]?.array {
            
            self.items = []
            for dic in items {
                
                let item = CollectionItem(parametersJson: dic.dictionary)
                self.items?.append(item)
            }
        }
    }
}
