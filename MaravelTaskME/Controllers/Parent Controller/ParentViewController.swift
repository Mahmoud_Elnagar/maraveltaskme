//
//  ParentViewController.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/19/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import MBProgressHUD

class ParentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showAlert(title: String?, message: String?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true)
    }
    
    func showMarvelLoader() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideMarvelLoader() {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
    
}
