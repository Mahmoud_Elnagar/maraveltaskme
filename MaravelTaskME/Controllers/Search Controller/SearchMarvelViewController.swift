//
//  SearchMarvelViewController.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class SearchMarvelViewController: ParentViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtSearch: UISearchBar!
    var searchedArray = [MarvelCharacter]()
    var presenter = SearchPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        txtSearch.showsCancelButton = true
        txtSearch.becomeFirstResponder()

        let attributes = [
            
            NSAttributedStringKey.foregroundColor : UIColor.red,
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let characterObj = searchedArray[indexPath.row]
        
        let lblName = cell.viewWithTag(1) as! UILabel
        let imgView = cell.viewWithTag(2) as! UIImageView
        
        
        lblName.text = characterObj.characterName!
        if let thumb = characterObj.thumbnail {
            
            if let imageUrl = URL.init(string: thumb){
                imgView.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "default"))
            }else {
                imgView.image = UIImage.init(named: "default")
            }
        }else {
            imgView.image = UIImage(named: "default")
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailsCTRL = self.storyboard?.instantiateViewController(withIdentifier: "CharacterDetailsViewController") as! CharacterDetailsViewController
        
        let character = searchedArray[indexPath.row]
        detailsCTRL.character = character
        
        self.navigationController?.pushViewController(detailsCTRL, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > 0 {
            self.getCharactersStartWith(searchText: searchText)
        }else {
            searchedArray = []
            self.tableView.reloadData()
        }
    }
    
    func getCharactersStartWith(searchText: String) {
        
        presenter.getAllCharacters(nameStartsWith: searchText, onSuccess: { (response) in
            if response.results!.count > 0 {
                self.searchedArray = response.results!
                self.tableView.reloadData()
            }
            
        }) {
            self.showAlert(title: "Error", message: "Oop, there is no available characters with your search.")
        }
    }
}
