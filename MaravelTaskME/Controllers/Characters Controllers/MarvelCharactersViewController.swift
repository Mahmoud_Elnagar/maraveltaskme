//
//  MarvelCharactersViewController.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class MarvelCharactersViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource {
    
    var charactersResponse = GetCharactersResponse()
    var charactersArray = [MarvelCharacter]()
    var presenter = CharactersPresenter()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.rowHeight = 180
        self.getCharacters()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return charactersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as! CharacterTableViewCell
        
        let character = charactersArray[indexPath.row]
        cell.handelCellData(character: character)
        
        if indexPath.row == charactersArray.count - 1 {
            self.getCharacters()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailsCTRL = self.storyboard?.instantiateViewController(withIdentifier: "CharacterDetailsViewController") as! CharacterDetailsViewController
        
        let character = charactersArray[indexPath.row]
        detailsCTRL.character = character
        
        self.navigationController?.pushViewController(detailsCTRL, animated: true)
    }
    
    func getCharacters() {
        
        self.showMarvelLoader()
        
        presenter.getAllCharacters(offset: charactersArray.count, onSuccess: { (response) in
            
            self.charactersResponse = response
            self.charactersArray.append(contentsOf: response.results!)
            self.tableView.reloadData()
            
            self.hideMarvelLoader()
        }) {
            self.hideMarvelLoader()
            self.showAlert(title: "Error", message: "Oop, there is no available more characters.")
        }
    }
    
    @IBAction func maravelSearchAction(_ sender: UIButton) {
        
        let searchCTRL = self.storyboard?.instantiateViewController(withIdentifier: "SearchMarvelViewController") as! SearchMarvelViewController
        
        self.navigationController?.pushViewController(searchCTRL, animated: true)
    }
}
