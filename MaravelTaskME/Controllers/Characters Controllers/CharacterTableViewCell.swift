//
//  CharacterTableViewCell.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/17/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SDWebImage

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func handelCellData(character: MarvelCharacter) {
        
        self.lbl_name.text = character.characterName
        
        if let imageUrl = URL.init(string: character.thumbnail!){
            self.imgView.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "default"))
        }else{
            self.imgView.image = UIImage.init(named: "default")
        }
    }
}
