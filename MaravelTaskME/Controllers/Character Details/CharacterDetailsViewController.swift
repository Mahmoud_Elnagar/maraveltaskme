//
//  CharacterDetailsViewController.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit
import SafariServices

class CharacterDetailsViewController: UIViewController, ReturnOpenURLDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let dataSource = DetailsTableDataSource()
    var character: MarvelCharacter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        lblTitle.text = character.characterName!
        
        dataSource.characterObject = self.character
        dataSource.delegate = self
        
        self.tableView.dataSource = dataSource
        self.tableView.delegate = dataSource
        self.tableView.reloadData()
        
        self.getDataForCharacter()
    }
    
    
    func getDataForCharacter() {
        
        for item in (character.comics?.items)! {
            
            if item.resourceURI != nil {
                DetailsPresenter().getImageThumbnailURL(url: item.resourceURI!, onSuccess: { (imageUrl) in
                    item.imageURL = imageUrl
                    
                    self.tableView.reloadData()
                    }, onFailure: {
                })
            }
        }
        
        for item in (character.events?.items)! {
            
            if item.resourceURI != nil {
                DetailsPresenter().getImageThumbnailURL(url: item.resourceURI!, onSuccess: { (imageUrl) in
                    item.imageURL = imageUrl
                    
                    self.tableView.reloadData()
                    }, onFailure: {
                })
            }
        }
        
        for item in (character.series?.items)! {
            
            if item.resourceURI != nil {
                DetailsPresenter().getImageThumbnailURL(url: item.resourceURI!, onSuccess: { (imageUrl) in
                    item.imageURL = imageUrl
                    
                    self.tableView.reloadData()
                    }, onFailure: {
                })
            }
        }
        
        for item in (character.stories?.items)! {
            
            if item.resourceURI != nil {
                DetailsPresenter().getImageThumbnailURL(url: item.resourceURI!, onSuccess: { (imageUrl) in
                    item.imageURL = imageUrl
                    
                    self.tableView.reloadData()
                    }, onFailure: {
                })
            }
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
        
    }

    func openURLWithURLString(urlString: String) {
        
        if let url = URL(string: urlString) {
            
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
}
