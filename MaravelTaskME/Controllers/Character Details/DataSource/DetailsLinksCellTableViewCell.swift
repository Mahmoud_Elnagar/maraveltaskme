//
//  DetailsLinksCellTableViewCell.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/19/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

protocol ReturnOpenURLDelegate {
    
    func openURLWithURLString(urlString: String)
}

class DetailsLinksCellTableViewCell: UITableViewCell {

    var detailsLink: String?
    var wikiLink: String?
    var comicLink: String?
    
    var delegate: ReturnOpenURLDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func openDetailsLink(_ sender: UIButton) {
        
        if delegate != nil {
            if detailsLink != nil {
                delegate?.openURLWithURLString(urlString: detailsLink!)
            }
        }
    }
    
    @IBAction func openWikiLink(_ sender: UIButton) {
        
        if delegate != nil {
            if wikiLink != nil {
                delegate?.openURLWithURLString(urlString: wikiLink!)
            }
        }
    }
    
    @IBAction func openComcicLink(_ sender: UIButton) {
        
        if delegate != nil {
            if comicLink != nil {
                delegate?.openURLWithURLString(urlString: comicLink!)
            }
        }
    }
}
