//
//  DetailsCollectionViewCell.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class DetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var characterImageView: UIImageView!    
    @IBOutlet weak var lblCharacterName: UILabel!
    
    func handelCellData(item: CollectionItem) {
        
        self.lblCharacterName.text = item.name!
        
        if item.imageURL != nil {
            if let imageUrl = URL.init(string: item.imageURL!){
                self.characterImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "default"))
            }else {
                self.characterImageView.image = UIImage.init(named: "default")
            }
        }else {
            self.characterImageView.image = UIImage(named: "default")
                
        }
        
    }
}
