//
//  DetailsTableDataSource.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class DetailsTableDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {

    var characterObject = MarvelCharacter()
    var delegate: ReturnOpenURLDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as! CharacterTableViewCell
            
            if let thumb = characterObject.thumbnail {
                
                if let imageUrl = URL.init(string: thumb){
                    cell.imgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "default"))
                }else {
                    cell.imgView.image = UIImage(named: "default")
                }
            }else {
                cell.imgView.image = UIImage(named: "default")
                
            }
            
            return cell
        }
        if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsData", for: indexPath)
            
            let lblName = cell.viewWithTag(1) as! UILabel
            let lblDesc = cell.viewWithTag(2) as! UILabel
            
            lblName.text = characterObject.characterName!
            lblDesc.text = characterObject.characterDescription!
            
            return cell
            
        }
        
        if indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCollectionTableViewCell", for: indexPath) as! DetailsCollectionTableViewCell
            
            cell.handelCellData(marvelCollection: characterObject.comics!, name: "Comics")
            
            return cell
            
        }
        
        if indexPath.row == 3 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCollectionTableViewCell", for: indexPath) as! DetailsCollectionTableViewCell
            
            cell.handelCellData(marvelCollection: characterObject.series!, name: "Series")
            
            return cell
            
        }
        
        if indexPath.row == 4 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCollectionTableViewCell", for: indexPath) as! DetailsCollectionTableViewCell
            
            cell.handelCellData(marvelCollection: characterObject.stories!, name: "Stories")
            
            return cell
            
        }
        
        if indexPath.row == 5 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsCollectionTableViewCell", for: indexPath) as! DetailsCollectionTableViewCell
            
            cell.handelCellData(marvelCollection: characterObject.events!, name: "Events")
            
            return cell
        }
        
        if indexPath.row == 6 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "linksCell", for: indexPath) as! DetailsLinksCellTableViewCell
            
            cell.delegate = self.delegate
            
            if characterObject.urls != nil {
                for urlType in characterObject.urls! {
                    if urlType.type == "detail" {
                        cell.detailsLink = urlType.url!
                    }
                    
                    if urlType.type == "wiki" {
                        cell.wikiLink = urlType.url!
                    }
                    
                    if urlType.type == "comiclink" {
                        cell.comicLink = urlType.url!
                    }
                }
            }
            
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as! CharacterTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 1 {
            if ((characterObject.characterDescription == nil) || (characterObject.characterDescription!.count == 0)) {
                return 120
            }
        }
        if ((characterObject.comics?.items?.count == 0) && indexPath.row == 2) {
           return 50
        }
        
        if ((characterObject.series?.items?.count == 0) && indexPath.row == 3) {
            return 50
        }
        
        if ((characterObject.stories?.items?.count == 0) && indexPath.row == 4) {
            return 50
        }
        
        if ((characterObject.events?.items?.count == 0) && indexPath.row == 5) {
            return 50
        }
        
        return 350
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
