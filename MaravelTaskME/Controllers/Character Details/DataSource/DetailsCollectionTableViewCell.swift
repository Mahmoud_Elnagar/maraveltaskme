//
//  DetailsCollectionTableViewCell.swift
//  MaravelTaskME
//
//  Created by Mahmoud ElNagar on 1/18/18.
//  Copyright © 2018 Mahmoud Elnagar. All rights reserved.
//

import UIKit

class DetailsCollectionTableViewCell: UITableViewCell {

    var items = [CollectionItem]()
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func handelCellData(marvelCollection: MarvelCollection, name: String) {
        
        self.items = marvelCollection.items!
        self.lbl_name.text = name
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.reloadData()
    }
}


extension DetailsCollectionTableViewCell : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "characterCell", for: indexPath) as! DetailsCollectionViewCell
        
        let item = self.items[indexPath.row]
        cell.handelCellData(item: item)
        
        return cell
    }
}


extension DetailsCollectionTableViewCell : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let height = collectionView.frame.height
        return CGSize(width:height * 0.6 , height: height)
        
    }
}
