//
//  ServiceManager.swift
//  Fashion
//
//  Created by Mahmoud Fathelbab on 10/26/17.
//  Copyright © 2017 FashionOnWheels. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceManager: NSObject {
    
    typealias ApiResponse = (Error?, JSON?) -> Void
    
    class func callAPI(url: String, method: HTTPMethod, parameters: [String: Any]?, custumHeaders: [String: String]?, onCompletion: @escaping ApiResponse) -> Void {
        
        Alamofire.request(URL(string: url)!, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON {response in
            
            print(response)
            if let result = response.result.value {
                
                onCompletion(nil, JSON(result));
            }else{
                onCompletion(response.result.error, nil);
            }
        }
    }
}
